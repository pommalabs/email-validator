# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import pytest
from hypothesis import given

from email_validator_app.settings import ValidatorSettings
from tests.utils import not_an_email_address_strategy

################################################################################
# Validator settings
################################################################################


@given(not_an_email_address_strategy())
def test_that_smtp_from_address_rejects_invalid_email_addresses(plain_text: str):
    # Act
    with pytest.raises(ValueError) as ex_info:
        ValidatorSettings(smtp_from_address=plain_text)
    assert "valid email address" in str(ex_info.value)
