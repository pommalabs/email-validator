# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from unittest.mock import Mock

from hypothesis import given
from litestar.di import Provide
from litestar.status_codes import HTTP_200_OK
from litestar.testing import create_test_client

from email_validator_app.controllers import ValidationController
from email_validator_app.models import ValidationComponent, ValidationResponse, Verdict
from email_validator_app.services import Validator
from tests.utils import email_address_strategy

VALIDATION_ENDPOINT = "/validate"


@given(email_address_strategy())
def test_that_validation_with_post_invokes_validator(address: str):
    # Arrange
    validator = Mock(spec_set=Validator)
    validator.validate.return_value = ValidationResponse(
        address=address,
        validator=ValidationComponent.PY3_VALIDATE_EMAIL,
        verdict=Verdict.VALID,
    )
    with create_test_client(
        route_handlers=ValidationController,
        dependencies={"validator": Provide(lambda: validator, sync_to_thread=False)},
    ) as client:
        # Act
        response = client.post(VALIDATION_ENDPOINT, json={"address": address})
        # Assert
        validator.validate.assert_called_once_with(address)
        assert response.status_code == HTTP_200_OK


@given(email_address_strategy())
def test_that_validation_with_get_invokes_validator(address: str):
    # Arrange
    validator = Mock(spec_set=Validator)
    validator.validate.return_value = ValidationResponse(
        address=address,
        validator=ValidationComponent.PY3_VALIDATE_EMAIL,
        verdict=Verdict.VALID,
    )
    with create_test_client(
        route_handlers=ValidationController,
        dependencies={"validator": Provide(lambda: validator, sync_to_thread=False)},
    ) as client:
        # Act
        response = client.get(VALIDATION_ENDPOINT, params={"address": address})
        # Assert
        validator.validate.assert_called_once_with(address)
        assert response.status_code == HTTP_200_OK
