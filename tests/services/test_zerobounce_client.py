# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.


import logging

import pytest
from dolcetto.services import get_logger
from dolcetto.testing import capture_logs_at_level
from hypothesis import given

from email_validator_app.services import ZeroBounceClient
from email_validator_app.settings import ZeroBounceSettings
from tests.utils import STUB_ZEROBOUNCE_API_KEY, email_address_strategy


def _zerobounce_client_factory(
    zerobounce_settings: ZeroBounceSettings = ZeroBounceSettings.load(),
):
    logger = get_logger()
    return ZeroBounceClient(logger, zerobounce_settings)


################################################################################
# Constructor
################################################################################


def test_that_constructor_does_not_log_about_zerobounce_availability_when_not_available(
    request: pytest.FixtureRequest,
):
    # Arrange
    zerobounce_settings = ZeroBounceSettings(api_key=None)
    with capture_logs_at_level(request, logging.INFO) as caplog:
        # Act
        _zerobounce_client_factory(zerobounce_settings=zerobounce_settings)
        # Assert
        assert len(caplog.messages) == 0


def test_that_constructor_logs_about_zerobounce_availability_when_available(
    request: pytest.FixtureRequest,
):
    # Arrange
    zerobounce_settings = ZeroBounceSettings(api_key=STUB_ZEROBOUNCE_API_KEY)
    with capture_logs_at_level(request, logging.INFO) as caplog:
        # Act
        _zerobounce_client_factory(zerobounce_settings=zerobounce_settings)
        # Assert
        assert len(caplog.messages) == 1


################################################################################
# Validate
################################################################################


@given(email_address_strategy())
def test_that_validate_raises_an_error_when_api_key_is_missing(address: str):
    # Arrange
    zerobounce_settings = ZeroBounceSettings(api_key=None)
    zerobounce_client = _zerobounce_client_factory(
        zerobounce_settings=zerobounce_settings
    )
    # Act
    with pytest.raises(ValueError) as ex_info:
        zerobounce_client.validate(address)
    # Assert
    assert "API key" in str(ex_info.value)


################################################################################
# Get credits
################################################################################


def test_that_get_credits_raises_an_error_when_api_key_is_missing():
    # Arrange
    zerobounce_settings = ZeroBounceSettings(api_key=None)
    zerobounce_client = _zerobounce_client_factory(
        zerobounce_settings=zerobounce_settings
    )
    # Act
    with pytest.raises(ValueError) as ex_info:
        zerobounce_client.get_credits()
    # Assert
    assert "API key" in str(ex_info.value)
