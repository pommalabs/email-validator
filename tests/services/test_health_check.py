# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from ipaddress import IPv4Address
from unittest.mock import patch

from dolcetto.services import get_logger
from healthcheck import HealthCheck
from hypothesis import given
from hypothesis import strategies as st
from litestar.status_codes import HTTP_200_OK, HTTP_503_SERVICE_UNAVAILABLE
from pydantic import AnyHttpUrl

from email_validator_app.models import ZeroBounceCreditsResponse
from email_validator_app.services import ZeroBounceClient, get_health_check
from email_validator_app.services import health_check as health_check_module
from email_validator_app.settings import HealthCheckSettings, ZeroBounceSettings
from tests.utils import STUB_ZEROBOUNCE_API_KEY


def _health_check_factory(
    health_check_settings: HealthCheckSettings = HealthCheckSettings.load(),
    zerobounce_settings: ZeroBounceSettings = ZeroBounceSettings.load(),
    zerobounce_client: ZeroBounceClient | None = None,
):
    logger = get_logger()
    zerobounce_client = zerobounce_client or ZeroBounceClient(
        logger, zerobounce_settings
    )
    return get_health_check(
        logger=logger,
        health_check_settings=health_check_settings,
        zerobounce_settings=zerobounce_settings,
        zerobounce_client=zerobounce_client,
    )


################################################################################
# Probe URLs
################################################################################


@given(st.lists(st.ip_addresses(v=4)))
def test_that_health_check_invokes_probe_url_for_each_url_to_be_probed(
    ip_addresses: list[IPv4Address],
):
    # Arrange
    urls = frozenset(
        [AnyHttpUrl(f"https://{ip_address}") for ip_address in ip_addresses]
    )
    health_check_settings = HealthCheckSettings(probe_urls=urls)
    health_check = _health_check_factory(health_check_settings=health_check_settings)
    with patch.object(
        health_check_module, "_probe_url", return_value=(True, "")
    ) as mock:
        # Act
        health_check.run()
        # Assert
        assert mock.call_count == len(urls)
        assert frozenset([call.args[0] for call in mock.call_args_list]) == urls


################################################################################
# Check ZeroBounce credits
################################################################################

ZEROBOUNCE_CREDITS_THRESHOLD = 100


def test_that_health_check_checks_zerobounce_credits_when_zerobounce_is_available():
    # Arrange
    health_check_settings = HealthCheckSettings(probe_urls=frozenset())
    zerobounce_settings = ZeroBounceSettings(api_key=STUB_ZEROBOUNCE_API_KEY)
    with patch.object(HealthCheck, "add_check") as mock:
        # Act
        _health_check_factory(
            health_check_settings=health_check_settings,
            zerobounce_settings=zerobounce_settings,
        )
        # Assert
        mock.assert_called_once()


def test_that_health_check_does_not_check_zerobounce_credits_when_zerobounce_is_not_available():
    # Arrange
    health_check_settings = HealthCheckSettings(probe_urls=frozenset())
    zerobounce_settings = ZeroBounceSettings(api_key=None)
    with patch.object(HealthCheck, "add_check") as mock:
        # Act
        _health_check_factory(
            health_check_settings=health_check_settings,
            zerobounce_settings=zerobounce_settings,
        )
        # Assert
        mock.assert_not_called()


@given(st.integers().filter(lambda i: i >= ZEROBOUNCE_CREDITS_THRESHOLD))
def test_that_health_check_returns_200_when_zerobounce_credits_check_succeeds(
    available_credits: int,
):
    # Arrange
    health_check_settings = HealthCheckSettings(probe_urls=frozenset())
    zerobounce_settings = ZeroBounceSettings(
        api_key=STUB_ZEROBOUNCE_API_KEY, credits_threshold=ZEROBOUNCE_CREDITS_THRESHOLD
    )
    health_check = _health_check_factory(
        health_check_settings=health_check_settings,
        zerobounce_settings=zerobounce_settings,
    )
    zerobounce_response = ZeroBounceCreditsResponse(credits=available_credits)
    with patch.object(
        ZeroBounceClient, "get_credits", return_value=zerobounce_response
    ):
        # Act
        (_, status_code, _) = health_check.run()
        # Assert
        assert status_code == HTTP_200_OK


@given(st.integers().filter(lambda i: i < ZEROBOUNCE_CREDITS_THRESHOLD))
def test_that_health_check_returns_503_when_zerobounce_credits_check_fails(
    available_credits: int,
):
    # Arrange
    health_check_settings = HealthCheckSettings(probe_urls=frozenset())
    zerobounce_settings = ZeroBounceSettings(
        api_key=STUB_ZEROBOUNCE_API_KEY, credits_threshold=ZEROBOUNCE_CREDITS_THRESHOLD
    )
    health_check = _health_check_factory(
        health_check_settings=health_check_settings,
        zerobounce_settings=zerobounce_settings,
    )
    zerobounce_response = ZeroBounceCreditsResponse(credits=available_credits)
    with patch.object(
        ZeroBounceClient, "get_credits", return_value=zerobounce_response
    ):
        # Act
        (_, status_code, _) = health_check.run()
        # Assert
        assert status_code == HTTP_503_SERVICE_UNAVAILABLE
