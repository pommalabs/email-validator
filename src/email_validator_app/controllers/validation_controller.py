# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from litestar import Controller, get, post
from litestar.exceptions import NotAuthorizedException
from litestar.params import Parameter
from litestar.status_codes import HTTP_200_OK

from email_validator_app.models import ValidationRequest, ValidationResponse
from email_validator_app.services import Validator


class ValidationController(Controller):
    path = "/validate"
    tags = ["email_validator"]

    @post(
        status_code=HTTP_200_OK,
        raises=[NotAuthorizedException],
        summary="Validates specified email address.",
    )
    async def validate(
        self, validator: Validator, data: ValidationRequest
    ) -> ValidationResponse:
        return validator.validate(data.address)

    @get(
        status_code=HTTP_200_OK,
        raises=[NotAuthorizedException],
        summary="Validates specified email address.",
        description="The usage of the `POST` endpoint is recommended.",
        deprecated=True,
    )
    async def deprecated_validate(
        self,
        validator: Validator,
        address: str = Parameter(
            description="The email address that is being validated."
        ),
    ) -> ValidationResponse:
        return validator.validate(address)
