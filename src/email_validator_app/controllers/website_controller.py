# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import binascii
from datetime import datetime, timezone
from typing import Any

from dolcetto import APP_VERSION
from dolcetto.models import FairUseToken
from dolcetto.services import FairUseTokenService
from litestar import Controller, Request, Response, get, post
from litestar.enums import RequestEncodingType
from litestar.exceptions import NotAuthorizedException
from litestar.params import Body
from litestar.response import Redirect, Template
from litestar.status_codes import HTTP_307_TEMPORARY_REDIRECT

from email_validator_app.models import ValidationResponse
from email_validator_app.services import Validator


class WebsiteController(Controller):
    path = "/"
    tags = ["website"]

    @get(include_in_schema=False)
    async def index(self, request: Request) -> Response:
        return self._index_template(None, request)

    @post(
        raises=[NotAuthorizedException],
        include_in_schema=False,
    )
    async def validate(
        self,
        validator: Validator,
        request: Request,
        fair_use_token_svc: FairUseTokenService,
        data: dict[str, Any] = Body(media_type=RequestEncodingType.URL_ENCODED),
    ) -> Response:
        validation_resp: ValidationResponse | None = None
        if self._validate_fair_use_token(data, fair_use_token_svc):
            validation_resp = validator.validate(data["address"])
        return self._index_template(validation_resp, request)

    @get("/docs", status_code=HTTP_307_TEMPORARY_REDIRECT, include_in_schema=False)
    async def redirect_to_docs(self) -> Redirect:
        return Redirect(path="/schema/swagger")

    def _index_template(
        self,
        validation_resp: ValidationResponse | None,
        request: Request,
    ) -> Template:
        return Template(
            "index.html",
            context={
                "validation_resp": validation_resp,
                "app_version": APP_VERSION.replace("+", "-"),
                "request_url": str(request.url),
                "current_year": datetime.now(timezone.utc).year,
            },
        )

    def _validate_fair_use_token(
        self, request_data: dict[str, Any], fair_use_token_svc: FairUseTokenService
    ) -> bool:
        token_field = "token"  # nosec B105 - This is not an hardcoded password.
        if token_field not in request_data:
            return False
        token: str = request_data[token_field]
        try:
            fair_use_token = FairUseToken(token=token)
            return fair_use_token_svc.token_is_valid(fair_use_token)
        except binascii.Error:
            # Token is not a valid Base64 string.
            return False
