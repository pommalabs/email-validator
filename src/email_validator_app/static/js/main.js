async function getFairUseToken() {
    const response = await fetch("/api/v1/fair-use-token");
    if (response.status === 429) {
        throw new Error("Validation rate limit has been exceeded. Please try again later.");
    }
    return await response.json();
}

async function onMainFormSubmit(e) {
    // Always prevent submit, fair use token should be requested first.
    e.preventDefault();

    try {
        const fairUseToken = await getFairUseToken();
        document.getElementById("token").value = fairUseToken.token;
    }
    catch (e) {
        alert(e);
        return;
    }

    // Disable the validate button and show the spinner.
    const validateBtn = document.getElementById("validate-btn");
    validateBtn.setAttribute("disabled", true);
    const validateSpinner = document.getElementById("validate-spinner");
    validateSpinner.classList.remove("d-none");

    // Fair use token has been obtained, listener should be removed
    // and submit needs to be triggered manually, again.
    this.removeEventListener("submit", onMainFormSubmit);
    this.submit();
}

window.addEventListener("load", function () {
    const mainForm = document.getElementById("main-form");
    mainForm.addEventListener("submit", onMainFormSubmit);
});
