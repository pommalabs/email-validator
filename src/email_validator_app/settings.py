# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from enum import StrEnum
from functools import cache
from ipaddress import IPv4Address, IPv6Address
from typing import Any

from dolcetto.settings import get_settings_config
from pydantic import AnyHttpUrl, PrivateAttr, field_validator
from pydantic.fields import Field
from pydantic_settings import BaseSettings, SettingsConfigDict
from validate_email.dns_check import AddressTypes
from validate_email.email_address import AddressFormatError, EmailAddress


class AddressType(StrEnum):
    IP_V4 = "IPv4"
    IP_V6 = "IPv6"


class ValidatorSettings(BaseSettings):
    model_config = get_settings_config(SettingsConfigDict(env_prefix="validator_"))

    check_format: bool = Field(
        default=True,
        description="Checks whether the email address has a valid structure."
        " Default value is True.",
    )
    check_blacklist: bool = Field(
        default=True,
        description="Checks the email address against a blacklist of domains."
        " Default value is True.",
    )
    check_dns: bool = Field(
        default=True, description="Checks the DNS MX-records. Default value is True."
    )
    dns_timeout: int = Field(
        default=10,
        description="Seconds until DNS timeout. Default value is 10 seconds.",
        gt=0,
    )
    check_smtp: bool = Field(
        default=True,
        description="Checks whether the email address actually exists"
        " by initiating an SMTP conversation. Default value is True.",
    )
    smtp_timeout: int = Field(
        default=10,
        description="Seconds until SMTP timeout. Default value is 10 seconds.",
        gt=0,
    )
    smtp_helo_host: str | None = Field(
        default=None,
        description="Host name to use in SMTP HELO/EHLO. If set to None (the default),"
        " the fully qualified domain name of the local host is used.",
    )
    smtp_from_address: str | None = Field(
        default=None,
        description="Email address used for the sender in the SMTP conversation."
        " If set to None (the default), email addresses to be validated"
        " will be used as the senders as well.",
    )
    smtp_skip_tls: bool = Field(
        default=False,
        description="Skips the TLS negotiation with the server, even when available."
        " Default value is False.",
    )
    smtp_debug: bool = Field(
        default=False,
        description="Activates smtplib's debug output which always goes to stderr."
        " Default value is False.",
    )
    address_types: frozenset[AddressType] = Field(
        default=frozenset([AddressType.IP_V4, AddressType.IP_V6]),
        description="The IP address types to use. Default value is IPv4 and IPv6."
        " Useful when SMTP communication happens through one interface only,"
        " but hosting server has a dual stack.",
    )
    cache_size: int = Field(
        default=256,
        description="Maximum size of validation response in-memory cache."
        " Default size is 256. Size can be set to zero to disable caching.",
        ge=0,
    )
    cache_ttl: int = Field(
        default=600,
        description="Time-to-live, specified in seconds, of validation response"
        " in-memory cache entries. Default TTL is 600 seconds.",
        gt=0,
    )

    _address_types: AddressTypes = PrivateAttr()

    def __init__(self, **data: Any):
        super().__init__(**data)
        self._init_address_types()

    @staticmethod
    @cache
    def load() -> "ValidatorSettings":
        return ValidatorSettings()

    @field_validator("smtp_from_address")
    @classmethod
    def validate_smtp_from_address(cls, value: str | None) -> str | None:
        if value is not None:
            try:
                EmailAddress(value)
            except AddressFormatError as err:
                raise ValueError("Must be a valid email address") from err
        return value

    def get_address_types(self) -> AddressTypes:
        return self._address_types

    def _init_address_types(self) -> None:
        address_types: list[type[IPv4Address] | type[IPv6Address]] = []
        if (
            AddressType.IP_V4
            in self.address_types  # pylint: disable=unsupported-membership-test
        ):
            address_types.append(IPv4Address)
        if (
            AddressType.IP_V6
            in self.address_types  # pylint: disable=unsupported-membership-test
        ):
            address_types.append(IPv6Address)
        self._address_types = frozenset(address_types)


class HealthCheckSettings(BaseSettings):
    model_config = get_settings_config(SettingsConfigDict(env_prefix="health_check_"))

    probe_urls: frozenset[AnyHttpUrl] = Field(
        default=frozenset([AnyHttpUrl("https://example.com/")]),
        description="URLs which should be probed to ensure"
        " an healthy internet connection is available."
        " Probe is not performed by sending an ICMP echo request"
        " because that protocol is not supported on many infrastructures;"
        ' instead, an HTTP "HEAD" request is sent to specified URLs.'
        " Default URL is example.com (https://example.com/).",
    )
    probe_timeout: int = Field(
        default=2,
        description="How many seconds should each URL probe last before failing."
        " Default value is 2 seconds, timeout value should be greater than zero.",
        gt=0,
    )

    @staticmethod
    @cache
    def load() -> "HealthCheckSettings":
        return HealthCheckSettings()


class ZeroBounceSettings(BaseSettings):
    model_config = get_settings_config(SettingsConfigDict(env_prefix="zerobounce_"))

    api_key: str | None = Field(
        default=None,
        description="API key used to interact with ZeroBounce.",
    )
    base_url: AnyHttpUrl = Field(
        default=AnyHttpUrl("https://api.zerobounce.net/v2"),
        description="ZeroBounce API base URL.",
    )
    credits_threshold: int = Field(
        default=1,
        description="If ZeroBounce credits drop below given threshold,"
        " then the web service health check will fail.",
    )
    timeout: int = Field(
        default=5,
        description="How many seconds should be used as timeout for ZeroBounce"
        " API HTTP requests",
    )

    @staticmethod
    @cache
    def load() -> "ZeroBounceSettings":
        return ZeroBounceSettings()

    def has_api_key(self) -> bool:
        return self.api_key is not None


class WebsiteSettings(BaseSettings):
    model_config = get_settings_config(SettingsConfigDict(env_prefix="website_"))

    enabled: bool = Field(
        default=False,
        description="Whether playground website is enabled or not. Defaults to false.",
    )

    @staticmethod
    @cache
    def load() -> "WebsiteSettings":
        return WebsiteSettings()
