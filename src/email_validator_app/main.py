# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import os

from cachetools import Cache, TTLCache
from dolcetto import (
    API_KEY_SECURITY_SCHEME,
    API_KEY_SECURITY_SCHEME_NAME,
    APP_DESCRIPTION,
    APP_VERSION,
    MIT_LICENSE,
    ApiKeyAuthenticationMiddleware,
    AppInfoController,
    HttpTracingMiddleware,
    logging_exception_handler,
    on_app_init,
    redirect_to_docs,
)
from dolcetto.controllers import FairUseTokenController
from litestar import Litestar, Router
from litestar.contrib.jinja import JinjaTemplateEngine
from litestar.di import Provide
from litestar.openapi import OpenAPIConfig
from litestar.openapi.spec import Components
from litestar.static_files import create_static_files_router
from litestar.status_codes import HTTP_500_INTERNAL_SERVER_ERROR
from litestar.template import TemplateConfig

from email_validator_app.controllers import ValidationController, WebsiteController
from email_validator_app.services import Validator, ZeroBounceClient, get_health_check
from email_validator_app.settings import (
    HealthCheckSettings,
    ValidatorSettings,
    WebsiteSettings,
    ZeroBounceSettings,
)

ROOT_PATH = os.path.dirname(__file__)
STATIC_FILES_PATH = f"{ROOT_PATH}/static"
TEMPLATES_PATH = f"{ROOT_PATH}/templates"

root = Router(
    path="/",
    route_handlers=[AppInfoController],
    dependencies={
        "health_check_settings": Provide(
            HealthCheckSettings.load, use_cache=True, sync_to_thread=False
        ),
        "health_check": Provide(get_health_check, use_cache=True, sync_to_thread=False),
    },
)

public_api_v1 = Router(
    path="/api/v1",
    route_handlers=[],
    middleware=[HttpTracingMiddleware.define()],
    exception_handlers={HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler()},
)

restricted_api_v1 = Router(
    path="/api/v1",
    route_handlers=[ValidationController],
    security=[{API_KEY_SECURITY_SCHEME_NAME: []}],
    middleware=[
        ApiKeyAuthenticationMiddleware.define(),
        HttpTracingMiddleware.define(),
    ],
    exception_handlers={HTTP_500_INTERNAL_SERVER_ERROR: logging_exception_handler()},
)

website_settings = WebsiteSettings.load()
website = Router(
    path="/",
    route_handlers=[
        create_static_files_router(path="/static", directories=[STATIC_FILES_PATH])
    ],
    middleware=[
        HttpTracingMiddleware.define(
            request_filter=lambda r: r.method.upper() == "POST"
        )
    ],
    dependencies={
        "website_settings": Provide(
            lambda: website_settings, use_cache=True, sync_to_thread=False
        )
    },
)

if not website_settings.enabled:
    root.register(redirect_to_docs)
else:
    public_api_v1.register(FairUseTokenController)
    website.register(WebsiteController)

validator_settings = ValidatorSettings.load()
validator_cache: Cache = TTLCache(
    validator_settings.cache_size, validator_settings.cache_ttl
)

app = Litestar(
    openapi_config=OpenAPIConfig(
        title="Email Validator",
        description=APP_DESCRIPTION,
        version=APP_VERSION,
        license=MIT_LICENSE,
        components=Components(
            security_schemes={API_KEY_SECURITY_SCHEME_NAME: API_KEY_SECURITY_SCHEME}
        ),
    ),
    route_handlers=[root, public_api_v1, restricted_api_v1, website],
    dependencies={
        "zerobounce_settings": Provide(
            ZeroBounceSettings.load, use_cache=True, sync_to_thread=False
        ),
        "zerobounce_client": Provide(
            ZeroBounceClient, use_cache=True, sync_to_thread=False
        ),
        "validator_settings": Provide(
            lambda: validator_settings, use_cache=True, sync_to_thread=False
        ),
        "validator_cache": Provide(
            lambda: validator_cache, use_cache=True, sync_to_thread=False
        ),
        "validator": Provide(Validator, use_cache=True, sync_to_thread=False),
    },
    on_app_init=[on_app_init],
    template_config=TemplateConfig(
        directory=TEMPLATES_PATH, engine=JinjaTemplateEngine
    ),
)
