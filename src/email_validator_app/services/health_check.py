# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import functools
import logging

from dolcetto.services import probe_url as _probe_url
from healthcheck import HealthCheck
from litestar.status_codes import HTTP_503_SERVICE_UNAVAILABLE
from pydantic import AnyHttpUrl

from email_validator_app.services.zerobounce_client import ZeroBounceClient
from email_validator_app.settings import HealthCheckSettings, ZeroBounceSettings


def probe_url(
    url: AnyHttpUrl,
    logger: logging.Logger,
    timeout: float,
) -> tuple[bool, str]:
    return _probe_url(url, logger, timeout)


def check_zerobounce_credits(
    logger: logging.Logger,
    zerobounce_settings: ZeroBounceSettings,
    zerobounce_client: ZeroBounceClient,
) -> tuple[bool, str]:
    zerobounce_response = zerobounce_client.get_credits()
    available_credits = int(zerobounce_response.credits)
    credits_threshold = zerobounce_settings.credits_threshold

    if available_credits >= credits_threshold:
        return (True, "OK")

    logger.warning(
        "Available ZeroBounce credits (%s) are below the configured threshold (%s)",
        available_credits,
        credits_threshold,
    )
    return (False, "KO")


def get_health_check(
    logger: logging.Logger,
    health_check_settings: HealthCheckSettings,
    zerobounce_settings: ZeroBounceSettings,
    zerobounce_client: ZeroBounceClient,
) -> HealthCheck:
    health_check = HealthCheck(failed_status=HTTP_503_SERVICE_UNAVAILABLE)

    for i, url in enumerate(health_check_settings.probe_urls, start=1):
        check = functools.partial(
            probe_url, url, logger, health_check_settings.probe_timeout
        )
        setattr(check, "__name__", f"{probe_url.__name__}_{i}")
        health_check.add_check(check)

    if zerobounce_settings.api_key:
        check = functools.partial(
            check_zerobounce_credits, logger, zerobounce_settings, zerobounce_client
        )
        setattr(check, "__name__", check_zerobounce_credits.__name__)
        health_check.add_check(check)

    return health_check
