# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging
import operator

import httpx
import pymailcheck
import validate_email
from cachetools import Cache, cachedmethod
from validate_email.email_address import AddressFormatError, EmailAddress
from validate_email.exceptions import (
    AddressNotDeliverableError,
    DNSConfigurationError,
    DNSTimeoutError,
    DomainBlacklistedError,
    DomainNotFoundError,
    EmailValidationError,
    NoMXError,
    NoNameserverError,
    NoValidMXError,
    SMTPCommunicationError,
    SMTPTemporaryError,
    TLSNegotiationError,
)

from email_validator_app.models import (
    ValidationComponent,
    ValidationResponse,
    Verdict,
    VerdictReason,
)
from email_validator_app.services.zerobounce_client import ZeroBounceClient
from email_validator_app.settings import ValidatorSettings, ZeroBounceSettings

CUSTOM_PYMAILCHECK_DOMAINS = pymailcheck.DOMAINS.union(
    [
        # Additional Italian free email service providers:
        "alice.it",
        "libero.it",
        "tiscali.it",
    ]
)

VERDICT_RULES = {
    # Format related errors:
    AddressFormatError.__name__: (Verdict.INVALID, "invalid_address_format"),
    # Domain and DNS related errors:
    DomainBlacklistedError.__name__: (Verdict.INVALID, "domain_blacklisted"),
    DomainNotFoundError.__name__: (Verdict.INVALID, "domain_not_found"),
    NoNameserverError.__name__: (Verdict.INVALID, "no_nameserver_found"),
    DNSTimeoutError.__name__: (Verdict.INVALID, "dns_timeout"),
    DNSConfigurationError.__name__: (Verdict.INVALID, "dns_configuration_error"),
    NoMXError.__name__: (Verdict.INVALID, "no_mx_record"),
    NoValidMXError.__name__: (Verdict.INVALID, "no_valid_mx_record"),
    # SMTP related errors:
    AddressNotDeliverableError.__name__: (Verdict.INVALID, "address_not_deliverable"),
    SMTPCommunicationError.__name__: (Verdict.INVALID, "smtp_communication_error"),
    SMTPTemporaryError.__name__: (Verdict.RISKY, "smtp_temporary_error"),
    TLSNegotiationError.__name__: (Verdict.RISKY, "tls_negotiation_error"),
}

ZEROBOUNCE_STATUS_RULES = {
    "valid": Verdict.VALID,
    "invalid": Verdict.INVALID,
    "catch-all": Verdict.VALID,
    "spamtrap": Verdict.INVALID,
    "abuse": Verdict.INVALID,
    "do_not_mail": Verdict.INVALID,
    "unknown": Verdict.RISKY,
}


class Validator:
    def __init__(  # pylint: disable=too-many-arguments,too-many-positional-arguments
        self,
        logger: logging.Logger,
        validator_settings: ValidatorSettings,
        validator_cache: Cache,
        zerobounce_settings: ZeroBounceSettings,
        zerobounce_client: ZeroBounceClient,
    ):
        self._logger = logger
        self._validator_settings = validator_settings
        self._validator_cache = validator_cache
        self._zerobounce_settings = zerobounce_settings
        self._zerobounce_client = zerobounce_client

    @cachedmethod(cache=operator.attrgetter("_validator_cache"))
    def validate(self, address: str) -> ValidationResponse:
        address = address.lower()

        # Try to parse email address, so that we can expose account and domain information.
        address_parsed, user, domain = self._parse_address(address)
        masked_address = Validator.mask_address(user, domain)
        self._logger.info('Validating email address "%s"', masked_address)

        # Validation is performed in two steps: a cheap one and an expensive one.
        # Following "cheap" validation checks that email address is formally correct
        # and that domain is correctly configured (it does exist, it has MX records, ...).
        # If this check passes, then the "expensive" one is performed.
        validator, verdict, reason = self._perform_formal_validation(address)

        # Validation is performed in two steps: a cheap one and an expensive one.
        # Following "expensive" validation checks that email address actually exists.
        # It relies on ZeroBounce, if API key has been specified, or it falls back
        # to "py3-validate-email" library, which talks to the SMTP servers of given email domain.
        if self._validator_settings.check_smtp and verdict is Verdict.VALID:
            validator, verdict, reason = self._perform_smtp_validation(address)

        # If verdict is not valid and given address is formally correct,
        # then we can try to suggest the user the correct address
        # by checking it against common email providers.
        suggestion = None
        if verdict is not Verdict.VALID and address_parsed:
            suggestion = self._suggest_alternative_address(address)

        self._logger.info(
            'Email address "%s" has been validated with verdict "%s",'
            ' reason code "%s" and suggestion "%s"',
            masked_address,
            verdict,
            reason.code if reason else "",
            self._mask_suggestion(suggestion),
        )
        return ValidationResponse(
            address=address,
            user=user,
            domain=domain,
            validator=validator,
            verdict=verdict,
            reason=reason,
            suggestion=suggestion,
        )

    @staticmethod
    def mask_address(user: str | None, domain: str | None) -> str | None:
        if user and domain:
            mask = "*"
            if len(user) == 1:
                return f"{mask}@{domain}"
            return f"{user[0]}{mask*(len(user)-1)}@{domain}"
        return None

    def _parse_address(self, address: str) -> tuple[bool, str | None, str | None]:
        try:
            parsed_address = EmailAddress(address)
            return True, parsed_address.user, parsed_address.domain
        except AddressFormatError:
            self._logger.warning("Email address could not be parsed", exc_info=True)
            return False, None, None

    def _mask_suggestion(self, suggestion: str | None) -> str | None:
        if not suggestion:
            return None
        parsed_suggestion = EmailAddress(suggestion)
        return Validator.mask_address(parsed_suggestion.user, parsed_suggestion.domain)

    def _handle_validation_error(
        self, error: EmailValidationError
    ) -> tuple[Verdict, VerdictReason | None]:
        self._logger.warning(
            "An error was raised while validating email address. %s",
            error,
            exc_info=True,
        )
        error_type_name = type(error).__name__
        if error_type_name in VERDICT_RULES:
            verdict, reason_code = VERDICT_RULES[error_type_name]
            reason = VerdictReason(code=reason_code, message=str(error))
            return verdict, reason
        return Verdict.RISKY, None

    def _perform_formal_validation(
        self, address: str
    ) -> tuple[ValidationComponent, Verdict, VerdictReason | None]:
        try:
            is_valid = validate_email.validate_email_or_fail(
                address,
                check_format=self._validator_settings.check_format,
                check_blacklist=self._validator_settings.check_blacklist,
                check_dns=self._validator_settings.check_dns,
                dns_timeout=self._validator_settings.dns_timeout,
                check_smtp=False,  # SMTP check is performed in the next step.
                address_types=self._validator_settings.get_address_types(),
            )
            verdict = Verdict.VALID if is_valid else Verdict.RISKY
            reason = None
        except EmailValidationError as err:
            verdict, reason = self._handle_validation_error(err)
        return ValidationComponent.PY3_VALIDATE_EMAIL, verdict, reason

    def _perform_smtp_validation(
        self, address: str
    ) -> tuple[ValidationComponent, Verdict, VerdictReason | None]:
        if self._zerobounce_settings.has_api_key():
            self._logger.info("Performing SMTP validation with ZeroBounce")
            return self._perform_zerobounce_smtp_validation(address)
        self._logger.info("Performing SMTP validation with py3-validate-email")
        return self._perform_local_smtp_validation(address)

    def _perform_local_smtp_validation(
        self, address: str
    ) -> tuple[ValidationComponent, Verdict, VerdictReason | None]:
        try:
            is_valid = validate_email.validate_email_or_fail(
                address,
                check_format=False,
                check_blacklist=False,
                check_dns=False,
                check_smtp=True,
                smtp_timeout=self._validator_settings.smtp_timeout,
                smtp_helo_host=self._validator_settings.smtp_helo_host,
                smtp_from_address=self._validator_settings.smtp_from_address,
                smtp_skip_tls=self._validator_settings.smtp_skip_tls,
                smtp_debug=self._validator_settings.smtp_debug,
                address_types=self._validator_settings.get_address_types(),
            )
            verdict = Verdict.VALID if is_valid else Verdict.RISKY
            reason = None
        except EmailValidationError as err:
            verdict, reason = self._handle_validation_error(err)
        return ValidationComponent.PY3_VALIDATE_EMAIL, verdict, reason

    def _perform_zerobounce_smtp_validation(
        self, address: str
    ) -> tuple[ValidationComponent, Verdict, VerdictReason | None]:
        try:
            zerobounce_response = self._zerobounce_client.validate(address)
            verdict = ZEROBOUNCE_STATUS_RULES[zerobounce_response.status]
            if (
                verdict is not Verdict.VALID
                and zerobounce_response.sub_status is not None
            ):
                reason = VerdictReason(
                    code=zerobounce_response.sub_status, message=None
                )
            else:
                reason = None
        except httpx.HTTPError:
            self._logger.error(
                "An error was raised while validating email address", exc_info=True
            )
            verdict = Verdict.RISKY
            reason = None
        return ValidationComponent.ZEROBOUNCE, verdict, reason

    def _suggest_alternative_address(self, address: str) -> str | None:
        suggest_result = pymailcheck.suggest(
            address, domains=CUSTOM_PYMAILCHECK_DOMAINS
        )
        if not suggest_result or suggest_result["full"] == address:
            return None
        suggestion = str(suggest_result["full"])
        try:
            EmailAddress(suggestion)
            return suggestion
        except AddressFormatError:
            # Suggest logic might generate invalid email addresses.
            # This has happened with IDNA encoded addresses, for example.
            return None
