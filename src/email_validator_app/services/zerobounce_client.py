# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import logging

import backoff
import httpx

from email_validator_app.models import (
    ZeroBounceCreditsResponse,
    ZeroBounceValidationResponse,
)
from email_validator_app.settings import ZeroBounceSettings


class ZeroBounceClient:
    def __init__(
        self,
        logger: logging.Logger,
        zerobounce_settings: ZeroBounceSettings,
    ):
        self._logger = logger
        self._zerobounce_settings = zerobounce_settings
        if self._zerobounce_settings.has_api_key():
            self._logger.info(
                "ZeroBounce API key has been specified. Client has been initialized"
                " and SMTP validation step will be performed using ZeroBounce Email Validator"
            )

    @backoff.on_exception(backoff.expo, httpx.HTTPError, max_tries=3)
    def validate(self, address: str) -> ZeroBounceValidationResponse:
        self._raise_error_when_api_key_is_missing()
        response = httpx.get(
            f"{self._zerobounce_settings.base_url}/validate",
            params={"api_key": self._zerobounce_settings.api_key, "email": address},
            timeout=self._zerobounce_settings.timeout,
        )
        response.raise_for_status()
        response_json = response.json()
        return ZeroBounceValidationResponse(
            status=response_json["status"], sub_status=response_json["sub_status"]
        )

    @backoff.on_exception(backoff.expo, httpx.HTTPError, max_tries=3)
    def get_credits(self) -> ZeroBounceCreditsResponse:
        self._raise_error_when_api_key_is_missing()
        response = httpx.get(
            f"{self._zerobounce_settings.base_url}/getcredits",
            params={"api_key": self._zerobounce_settings.api_key},
            timeout=self._zerobounce_settings.timeout,
        )
        response.raise_for_status()
        response_json = response.json()
        return ZeroBounceCreditsResponse(credits=response_json["Credits"])

    def _raise_error_when_api_key_is_missing(self) -> None:
        if not self._zerobounce_settings.has_api_key():
            raise ValueError("Missing ZeroBounce API key")
