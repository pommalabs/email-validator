# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from enum import StrEnum

from dolcetto.models import get_model_config
from pydantic import BaseModel, Field


class ValidationRequest(BaseModel):
    """The request of the email address validation process."""

    model_config = get_model_config()

    address: str = Field(description="The email address that is being validated.")


class ValidationComponent(StrEnum):
    """The component that produced the email address validation verdict."""

    PY3_VALIDATE_EMAIL = "py3_validate_email"
    ZEROBOUNCE = "zerobounce"


class Verdict(StrEnum):
    """
    Describes the three possible outcomes of the validation process.
    A valid verdict means that the email address is very likely to be existing.
    A risky verdict means that transient errors undermined the validation process.
    An invalid verdict means that the email address is very likely to be wrong or not existing.
    """

    VALID = "valid"
    RISKY = "risky"
    INVALID = "invalid"


class VerdictReason(BaseModel):
    """The details about why the email address was classified as risky or invalid."""

    model_config = get_model_config()

    code: str = Field(
        description="Error code."
        " Please check project README for further information."
    )
    message: str | None = Field(description="Descriptive error message.")


class ValidationResponse(BaseModel):
    """The response of the email address validation process."""

    model_config = get_model_config()

    address: str = Field(description="The email address that is being validated.")
    user: str | None = Field(
        default=None,
        description='The portion of the email address before the "@" symbol.',
    )
    domain: str | None = Field(
        default=None,
        description='The portion of the email address after the "@" symbol.',
    )
    validator: ValidationComponent = Field(
        description="The component that created the validation result."
        " It can assume the following values: py3_validate_email, zerobounce.",
    )
    verdict: Verdict = Field(
        description="A generic classification of whether or not the email address is valid."
        " It can assume the following values: valid, risky, invalid.",
    )
    reason: VerdictReason | None = Field(
        default=None,
        description="When verdict is risky or invalid, the reason why the email address"
        " was marked as such is exposed as an error code and message.",
    )
    suggestion: str | None = Field(
        default=None,
        description="A suggested correction in the event of domain name typos (e.g., gmial.com).",
    )


class ZeroBounceValidationResponse(BaseModel):
    """Internal model used to map ZeroBounce validation response."""

    model_config = get_model_config()

    status: str
    sub_status: str | None = None


class ZeroBounceCreditsResponse(BaseModel):
    """Internal model used to map ZeroBounce credits response."""

    model_config = get_model_config()

    credits: int
