# Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

from dolcetto.scripting import (
    generate_fair_use_token_key as _generate_fair_use_token_key,
)
from dolcetto.scripting import git_version as _git_version
from dolcetto.scripting import start as _start
from dolcetto.scripting import test as _test

PROJECT_MODULE = "email_validator_app"


def git_version():
    _git_version(auto_increment=True)


def start():
    _start(PROJECT_MODULE)


def test():
    _test(PROJECT_MODULE)


def generate_fair_use_token_key():
    _generate_fair_use_token_key()
